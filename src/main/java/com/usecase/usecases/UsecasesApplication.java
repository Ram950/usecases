package com.usecase.usecases;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsecasesApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsecasesApplication.class, args);
	}

}
